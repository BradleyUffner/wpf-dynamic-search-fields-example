﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicViewExample.Models
{
    class Widget
    {
        public string ModelNumber { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
